// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : { //הוספנו את כל הקונפיגורציה של פיירבייס
    apiKey: "AIzaSyBUSiJLvrg-x5Dv4f53jYYlZcbUxuvKHx4",
    authDomain: "hello-6781d.firebaseapp.com",
    databaseURL: "https://hello-6781d.firebaseio.com",
    projectId: "hello-6781d",
    storageBucket: "hello-6781d.appspot.com",
    messagingSenderId: "1042666762030",
    appId: "1:1042666762030:web:292e1477028430768f54bf"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
