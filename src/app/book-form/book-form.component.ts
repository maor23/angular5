import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'; //הוספנו את האינפוט הרצאה 11
//import { EventEmitter } from 'events'; //הרצאה 11 חשוב שהאמיטר לא יילקח מהספרייה הזאת, אלא מהסיפרייה בשורה למעלה
import { Book } from '../interfaces/book'; //הרצאה 11

@Component({
  selector: 'bookform', //שינינו הרצאה 11 סתם בשביל שיהיה שם יותר נוח להטמיע בטייפלייט
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})

export class BookFormComponent implements OnInit {

  //נגדיר משתנים כאינפוט כמשתנים של המחלקה
  //נגדיר את האינפוטים הרצאה 11
  @Input() title: string;
  @Input() author: string;
  @Input() id: string;
  @Input() formType: string; //הרצאה 12 משתנה בשביל כותרת הטופס שישתנה
  //נגדיר אווטפוט הרצאה 11
  @Output() update = new EventEmitter<Book>(); //חובה שהמשתנה אפדייט יהיה מסוג איבנטאמיטר - כדי שהבן יוכל לשדר נתונים לאב
  @Output() closeEdit = new EventEmitter<null>(); //נשים נאל כי לא מעבירים איזשהו מידע, רק מעדכנים את אלמנט האב שהאירוע התרחש
  //@Output() add = new EventEmitter<Book>(); //תרגיל בית 11

  constructor() { }

  //פונקצייה ששולחת את הנתונים ומעבירה מידע לאמלנט האב
  updateParent(){
    let book:Book = {id:this.id,title:this.title,author:this.author};
    this.update.emit(book); //אמיט זאת פונקצייה שמורה של אנגולר, שתפקידה לשדר מידע החוצה. כך הבן משדר מידע לאב
    if (this.formType == "Add Book"){ //נוסיף כדי שאם הטופס הוא טופס הוספה, אז שלא יהיו בו ערכים אלא שייפתח כטופס ריק
      this.title = null;
      this.author = null;
    }
  }



  //פונקצייה שתפקידה רק לעדכן את אמלנט האב ולא להעביר מידע או משהו
  tellParenttoClose(){
    this.closeEdit.emit(); //לא נשים פרמטר בתוך האמיט' כי לא מעבירים מידע לאלמנט האב רק מעדכנים אותו
  }


  
  ngOnInit(): void {
  }

}
