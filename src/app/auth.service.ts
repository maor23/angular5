import { Observable } from 'rxjs'; //תרגיל בית 8
import { Injectable } from '@angular/core';
import{ AngularFireAuth } from '@angular/fire/auth'; // הוספנו
import { Router } from '@angular/router'; // הוספנו
import { User } from './interfaces/user'; // הוספנו



@Injectable({
  providedIn: 'root'
})

export class AuthService {

  //הרצאה 8
  constructor(private afAuth:AngularFireAuth, private router:Router) { //נזריק סרביס של ספרייה של פיירבייס בשביל כל תהליך האימות ההתחברות 
    this.user = this.afAuth.authState; // אוטסטייט זה אובסרבל ולא פונקציה - זה טוב בגלל שהמשתנה יוזר שייצרנו הוא מסוג אובסרבל. כדי להוציא את המידע ממנו נצטרך לעשות גם לעשות סאסקרייב בטיימפלייט 
   } 

  //הרצאה 8
  user:Observable<User | null>; // ניצור משתנה מסוג אובסרבל, ובאותו אופן כמו שרשום באינטרפייס של יוזר הרצאה 8
  

   //הרצאה 7 פונקציית התחברות
  login(email:string, password:string){ //נוסיף פונקציית לוג אין שתקבל אימייל וסיסמא ותבדוק מול פיירבייס
    return this.afAuth.signInWithEmailAndPassword(email,password); //פונקצייה שמורה שמחזירה פרומיס שזה דומה לאוברסרבל
  }


  //הרצאה 8 - פונקציית התנתקות
  logout(){
    this.afAuth.signOut();
  }

  //הרצאה   פונקצייה שמביאה לנו את היוזר שהוא מחובר ותחזיר נאל אם אין יוזר מחובר8
  getUser():Observable <User | null> { //פונקצייה שתחזיר לנו אוברסרבל של יוזר ולא את היוזר עצמו
    return this.user;
  }

  //הרצאה 9 פונקציית הרשמה
  //הסרביס אמור רק להחזיר אובסרבול או פרומיס לקומפוננט וזהו לכן את כל הודעות הגיאה והמעבר לבוקס נעביר לקומפוננט
  SignUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password); //פונקצייה שמורה שמחזירה פרומיס שזה דומה לאוברסרבל, נחזיר את הפרומיס
  }

  


  
}





  



  
  /*
  //תרגיל בית 7
  //נוסיף פונקציה של הרשמה
  signupUser(user: User) { //מקבלים נתונים מהסוג של האינטרפייס יוזר
    this.afAuth
    .createUserWithEmailAndPassword(user.email, user.password) 
    .then(res => { 
      console.log(res); //נדפיס את התשובה לקונסול
      this.router.navigate(['/books']); //אם ההרשמה בוצעה בהצלחה יהיה מעבר לבוקס
  })
}
*/




