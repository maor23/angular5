import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component'; // הוספנו ייבוא לקומפוננט שהוספנו
import { CityFormComponent } from './city-form/city-form.component';
import { ClassifyComponent } from './classify/classify.component'; // הוספנו ייבוא לקומפוננט שהוספנו
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { RegisterComponent } from './register/register.component';
import { TemperaturesComponent } from './temperatures/temperatures.component'; // הוספנו ייבוא לקומפוננט שהוספנו

const routes: Routes = [ // מערך של ראוטים
  { path: 'books', component: BooksComponent }, // הוספנו - מגדירים את שם היו אר אל ואת הקומפוננט שנרצה להציג
  { path: 'temperatures/:city', component: TemperaturesComponent }, //הוספנו פרמטר חובה הרצאה 4
  { path: 'classify/:network', component: ClassifyComponent }, //הוספנו תרגיל בית 3, והוספנו פרמטר חובה בראוט בתרגיל בית 4
  { path: 'city', component: CityFormComponent }, //הראצה 6
  { path: 'posts', component: PostsComponent }, //תרגיל בית 6
  { path: 'login', component: LoginComponent }, //הרצאה 7
  { path: 'signup', component: RegisterComponent }, //תרגיל בית 7
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
