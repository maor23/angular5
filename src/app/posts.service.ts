import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Posts } from './interfaces/posts';


@Injectable({
  providedIn: 'root'
})


export class PostsService {

  constructor(private http:HttpClient) { } //ניצור משתנה בשם אייצ'טיטיפי מסוג אייצ'טיטיפי קליינט 

  
  private URL = "https://jsonplaceholder.typicode.com/posts/"; //היואראל משם לוקחים את הנתונים

  
  private handleError(res:HttpErrorResponse){ // פונקציה שברגע שיש שגיאה מפנים אליה והיא מטפלת בשגיאות
    console.log(res.error); //error שיצרנו יש שדה בשם res ראשית נרשום את השגיאות לקונסול בעזרת הפונקציה לוג. לאובייקט 
    return throwError (res.error || 'Server error'); //לפחות שתיהיה ההודעה סרבר ארור res נחזיר את השגיאה, ולמקרה שהגורם החיצוני לא טרח לכתוב את ההערה בתוך  
  }  
  

  searchPostData():Observable<Posts>{ // פונקציה שמבצעת בפועל את ההתקשרות עם השרת, הפלט יהיה אובסרבל שימשוך נתונים מסוג האינטרפייס פוסטס
    return this.http.get<Posts>(`${this.URL}`).pipe( //נפעיל פונקציית פייפ על האוברסבל
      catchError(this.handleError)
      )
  }
  

}
