import { HttpClient, HttpErrorResponse } from '@angular/common/http'; //התווסף אוטומטית כשרשמנו בתוך הקונסטרקטור
import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs'; //הוספנו
import { catchError, map } from 'rxjs/operators'; //הוספנו
import { Weather } from './interfaces/weather';//הוספנו
import { WeatherRaw } from './interfaces/weather-raw'; //הוספנו


@Injectable({
  providedIn: 'root'
})

export class WeatherService { //נוסיף מהאתר של המזג אוויר - הרצאה 4
  //התחברות לקריאת האייפיאיי
  private URL = "https://api.openweathermap.org/data/2.5/weather?q="; //האנדפוינט למעשה
  private KEY = "255bdf79d407ade14eb04f8820fab228"; //האייפיאיי קיי האישי שלי
  private IMP = "units=metric"; //בשביל היחידות

  constructor(private http:HttpClient) { } //נעשה אייפיאיי קולס, בכך שניצור משתנה בשם אייצטיטיפי שיהיה מסוג איצצטיטיפי קליינט


  
    //"נבנה פונקציה שממירה את הנתונים הגולמיים מ-"ווטר-ראו" למבנה הנתונים שאנחנו רוצים ב-"ווטר
    private transformWeatherData(data:WeatherRaw):Weather{ //הקלט הם נתונים מסוג ווטר-ראו והפלט הם נתונים מסוג ווטר
      return {
        name:data.name, //"הניים של "ווטר", יהיה בעצם הדאטה נקודה ניים, שב-"ווטר ראו
        country:data.sys.country, //נגיע לקאונטרי מהנתונים שבקובץ ווטר-ראו לפי הסדר שלהם: דאטה, ואז אס ווי אס, ואז קאונטרי
        image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`, //כדי להגיע לתמונה של האייקון (בתוך המערך של ה"ווטר" בקובץ ווטר-ראו), נשתול ערכים בתוך היואראל
        description:data.weather[0].description, //נגיע לדיסקריפשיין, שבתוך המערך של ה"ווטר", בקובץ ווטר-ראו
        temperature:data.main.temp, //כאן השם של המשתנה שנרצה שיהיה הוא "טמפרטורה" כמו שמופיע בווטר, אבל הוא לוקח מהנתון "טמפ" כמו שמופיע בבווטר-ראו
        lat:data.coord.lat,//נוסיף גם את המשתנים האופציונליים
        lon:data.coord.lon //נוסיף גם את המשתנים האופציונליים
      }
    }
  
  
  
  // פונקציה שברגע שיש שגיאה מפנים אליה והיא מטפלת בשגיאות
  private handleError(res:HttpErrorResponse){ 
    console.log(res.error); //error שיצרנו יש שדה בשם res ראשית נרשום את השגיאות לקונסול בעזרת הפונקציה לוג. לאובייקט 
    return throwError (res.error || 'Server error'); //"לפחות שתיהיה ההודעה "סרבר ארור res נחזיר את השגיאה, ולמקרה שהגורם החיצוני לא טרח לכתוב את ההערה בתוך  
  }  
  
  

// פונקציה שמבצעת בפועל את ההתקשרות עם השרת - קריאת אייפיאיי
  searchWeatherData(cityName:string):Observable<Weather>{ //הקלט הוא שם של עיר, והפלט יהיה אובסרבל שהמידע שיהיה לו בפנים יהיה מהסוג נתונים של האינטרפייס ווטר
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe( //נשתמש בפייפ כדי שיהיה ניתן להפעיל פונקציות על האובסרבל שנוצר
      map(data => this.transformWeatherData(data)), //נפעיל פונקציות על האובסרבל - את הפונקצייה שבנינו שממירה נתונים, מאפ זאת פונקציה שמורה
      catchError(this.handleError) // נפעיל על האובסרבל פונקצייה שבנינו (נוסיף אימפורט למעלה גם), קאצ'ארור זאת פונקציה שמורה
    )
  }


  

}



