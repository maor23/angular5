import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css']
})

export class CityFormComponent implements OnInit {

  constructor(private router:Router) { } //ניצור משתנה ראוטר מסוג ראוטר
  
  //נגדיר משתנים
  cities:Object[] = [{id:1,name:'Jerusalem'},{id:2,name:'London'},{id:3,name:'Paris'},{id:4,name:'BongaRRRR'}] //העיר האחרונה לא קיימת בכוונה
  city:string;

  
  onSubmit(){
    this.router.navigate(['/temperatures',this.city]) //הפונקציה שתפעל ברגע שנלחץ על הסאבמיט בטופס, נשלח את המשתנה סיטי שייבחר ליואראל של טמפרטורה
  }
  

  ngOnInit(): void {
  }

}
