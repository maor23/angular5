import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //השם הזה של הסלקטור נמצא בעמוד של אינדקס נקודה אייצטיאאל שהוא העמוד הראשי של האפליקציה 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'hello'; //משתנה = תכונה 
}
