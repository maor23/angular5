import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Book } from '../interfaces/book';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

export class BooksComponent implements OnInit { 

  
  
  constructor(private booksService:BooksService, public authService:AuthService) { } //ניצור דיפטנדנסי אינג'קשיין
  
  //נגדיר משתנים
  panelOpenState = false;  //הוספנו בשביל התפריט אקורדיון לכל ספר בהרצאה 3
  //books; // public תכונה שרושמים בצורה הזאת היא בעצם  
  books$; // הדולר מסמל את העובדה שזה אובסרבאבל - מוסכמה של אנגולר שאומרת שלמשתנה הזה נחבר אובסרבל
  userId:string;//הרצאה 10
  editstate = []; //הרצאה 11. נגדיר את המשתנה כמערך, בשביל שכל אלמנט של ספר יושפע ספציפית לגביו, ושלא ישפיע על שאר הספרים 
  addBookFormOpen = false; //הרצאה 12 משתנה שנאתחל כפאלס בשביל שלא יפתח את הטופס של הוספת הספר
  books:Book[]; //הרצאה 12 נגדיר משתנה של מערך של ספרים
  lastDocumentArrived; //הרצאה 12
  firstDocumentArrived; //תרגיל בית 12
  
  ngOnInit(): void {  
    //הרצאה 12
    //העברנו חלק מהפונקציונליות מהפונקצייה המקורית של גט בוקס מהסרביס לכאן
    this.authService.getUser().subscribe( //ניעזר בפונקציה שמחזירה את האיידי של היוזר המחובר שרשמנו באוט'סרביס
    user =>{
      this.userId = user.uid; //נקרא ליוזר שמחובר
      this.books$ = this.booksService.getBooks(this.userId,null); //נקרא לפונקציה גטבוקס שכתבנו בסרביס, וניצור את האובסרבל
      this.books$.subscribe( //נרשם לאובסרבול
        docs => { 
          this.lastDocumentArrived = docs[docs.length-1].payload.doc; //כדי להגיע לאלמנט האחרון שהגיע
          this.books = []; //נאפס את מערך הספרים
          for(let document of docs){ //ונבנה מחדש את מערך הספרים בעזרת לולאה, כך שכל פעם שנקבל דיווח מהסנאפשוט מהסרביס שיש שינוי בקולייקשן בדאטה בייס, נעדכן את המערך של רשימת הספרים , ובעזרת לולאה נכניס את המשתנים לספרים
            const book:Book = document.payload.doc.data(); //כדי שנוציא דאטה מתוך הדוקומנט ושנדע מה הגוף של כל דוקומנט (במקרה של בוקס זה טייטל וסופר)
            book.id = document.payload.doc.id; //האיידי הוא לא חלק מהדאטה, אותו צריך להביא בנפרד
            this.books.push(book); //נוסיף למערך הריק שאיפסנו מקודם, את כל הספרים
          } 
        }
      )
    }
  )
}


//פונקצייה שבאמצעותה אנחנו נבצע את הדפדוף לעמוד הבא
 // פונקצייה שתיהיה דומה לפונקציה של אוןאיניט, רק בלי הקטע של היוזר, כי ברגע שאנחנו כבר הולכים לעמוד הבא יש לנו כבר את היוזר איידי
  nextPage(){
      this.books$ = this.booksService.getBooks(this.userId, this.lastDocumentArrived); //נקרא לפונקציה גטבוקס שכתבנו בסרביס, וניצור את האובסרבל
      this.books$.subscribe( //נרשם לאובסרבול
        docs => { 
          this.lastDocumentArrived = docs[docs.length-1].payload.doc; //כדי להגיע לאלמנט האחרון שהגיע
          this.firstDocumentArrived = docs[0].payload.doc; //תרגיל בית 12 כדי להגיע לאלמנט הראשון שהגיע
          this.books = []; //נאפס את מערך הספרים
          for(let document of docs){ //ונבנה מחדש את מערך הספרים בעזרת לולאה, כך שכל פעם שנקבל דיווח מהסנאפשוט מהסרביס שיש שינוי בקולייקשן בדאטה בייס, נעדכן את המערך של רשימת הספרים , ובעזרת לולאה נכניס את המשתנים לספרים
            const book:Book = document.payload.doc.data(); //כדי שנוציא דאטה מתוך הדוקומנט ושנדע מה הגוף של כל דוקומנט (במקרה של בוקס זה טייטל וסופר)
            book.id = document.payload.doc.id; //האיידי של כל ספר לא נחשב חלק מהדאטה לכן נביא אותו בנפרד
            this.books.push(book); //נוסיף למערך הריק שאיפסנו מקודם, את כל הספרים
          } 
        }
      )
    }


 // תרגיל בית 12
 previewsPage(){
  this.books$ = this.booksService.getBooks2(this.userId, this.firstDocumentArrived); //נקרא לפונקציה גטבוקס שכתבנו בסרביס, וניצור את האובסרבל
  this.books$.subscribe( //נרשם לאובסרבול
    docs => { 
      this.lastDocumentArrived = docs[docs.length-1].payload.doc; //כדי להגיע לאלמנט האחרון שהגיע
      this.firstDocumentArrived = docs[0].payload.doc; //כדי להגיע לאלמנט הראשון שהגיע
      this.books = []; //נאפס את מערך הספרים
      for(let document of docs){ //ונבנה מחדש את מערך הספרים בעזרת לולאה, כך שכל פעם שנקבל דיווח מהסנאפשוט מהסרביס שיש שינוי בקולייקשן בדאטה בייס, נעדכן את המערך של רשימת הספרים , ובעזרת לולאה נכניס את המשתנים לספרים
        const book:Book = document.payload.doc.data(); //כדי שנוציא דאטה מתוך הדוקומנט ושנדע מה הגוף של כל דוקומנט (במקרה של בוקס זה טייטל וסופר)
        book.id = document.payload.doc.id; //האיידי של כל ספר לא נחשב חלק מהדאטה לכן נביא אותו בנפרד
        this.books.push(book); //נוסיף למערך הריק שאיפסנו מקודם, את כל הספרים
      } 
    }
  )
}





    //הרצאה 4 - נמשוך את הספרים מתוך הסרביס
    //this.books = this.booksService.getBooks(); //נקרא לפונקצייה גט בוקס שרשמנו בסרביס כדי למשוך את הספרים
    //this.books$ = this.booksService.getBooks(); //נקרא לפונקצייה גט בוקס השנייה שרשמנו בסרביס (זאת עם האובסרבל) כדי למשוך את הספרים
    //אפשר להוריד את השורה הזאת כי הכנסנו אייסינק פייפ לאיג'טיאמאל  ועכשיו הרישום יהיה משם ולא מפה
    //this.books$.subscribe(books => this.books = books); // נבצע את הרישום לאובסרבל מקובץ הטי אס
    //this.bookService.addBooks(); //נקרא לפונקצייה אד בוק מהסרביס - ויתווסף ספר חדש כל 2 שניות בעזרת שינוי באובסרבל

    /*
    //הרצאה 10
    this.authService.getUser().subscribe( //ניעזר בפונקציה שמחזירה את האיידי של היוזר המחובר שרשמנו באוט'סרביס
      user =>{
        this.userId = user.uid; //נקרא ליוזר שמחובר
        this.books$ = this.booksService.getBooks(this.userId); //נקרא לפונקציה גטבוקס שכתבנו בסרביס, וניצור את האובסרבל
      }
    )
  }
  */




  //הרצאה 10 פונקצייה שתפעיל את המחיקת ספר שעשינו בסרביס 
  deleteBook(id:string){ // הפונקציה תקבל את האיידי של הספר מהטיימפלייט (כי הממשתמש בוחר איזה ספר למחוק)
    this.booksService.deleteBook(this.userId,id); //נקרא לפונקציה מהסרביס שצריך גם להוסיף לה את האיידי של היוזר
  }



  //הרצאה 11 פונקציית עדכון שתפעיל את הפונקציה שכתבנו בסרביס
  //עדיף לתת אותו שם לפונקציה כמו השם שנתנו לאווטפוט בקומפוננט הבן
  update(book:Book){
    this.booksService.updateBook(this.userId,book.id,book.title,book.author);
  }

  //הרצאה 12
  //פונקציית הוספת ספר שתפעיל את הפונקציה שכתבנו בסרביס
  add(book:Book){
      this.booksService.addBook(this.userId,book.title,book.author);
    }





}
