import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router'; //תרגיל בית 9

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(private authService:AuthService, private router:Router) { } //נייצר הזרקה של הסרביס עבור הקומפוננט של לוגאין

  //נגדיר משתנים
  hide = true; //תרגיל בית 8 נוסיף משתנה בשביל העין של הסיסמא בטופס
  email:string;
  password:string;
  errorMessage:string; //הרצאה 9
  isError:boolean = false; //הרצאה 9


  //תרגיל בית 9
  //נעביר לקומפוננט כאן במקום מהסרביס את כל נושא הודעות השגיאה והמעבר לבוקס אחרי ההתחברות 
  onSubmit(){ // נבנה פונקציה של מה קורה אחרי לחיצת הכפתור שבטופס התחברות
    this.authService.login(this.email, this.password)
    .then(res => {  //  then במקום סאסקרייב לפרומיס יש את 
          console.log(res); //נדפיס את התשובה לקונסול
          this.router.navigate(['/books']) //אם הלוג אין בוצע בהצלחה יהיה מעבר לבוקס 
        })
      .catch(err => { //בשביל הודעות השגיאה
          console.log(err);
          this.isError = true;
          this.errorMessage = err.message;
        }) 
    }

  
  
  ngOnInit(): void {
  }



}
