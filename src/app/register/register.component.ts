import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; //הרצאה 9
//import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; //תרגיל בית 7
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit { 

  constructor(private authService:AuthService, private router:Router) { }

  //נגדיר משתנים
  hide = true; //תרגיל בית 8 נוסיף משתנה בשביל העין של הסיסמא בטופס
  email:string;
  password:string;
  errorMessage:string; //הרצאה 9
  isError:boolean = false; //הרצאה 9
  
  
  //הרצאה 9
  //נעביר לקומפוננט כאן במקום מהסרביס את כל נושא הודעות השגיאה והמעבר לבוקס אחרי ההרשמה 
  onSubmit(){ //הרצאה 8 נבנה פונקציה של מה קורה אחרי לחיצת הכפתור שבטופס הרשמה
    this.authService.SignUp(this.email, this.password)
    .then(res => {   //  then במקום סאסקרייב לפרומיס יש את 
          console.log(res); //נדפיס את התשובה לקונסול
          this.router.navigate(['/books']) //אם ההרשמה בוצעה בהצלחה יהיה מעבר לבוקס 
        })
      .catch(err => { //בשביל הודעות השגיאה
          console.log(err);
          this.isError = true;
          this.errorMessage = err.message;
        }) 
    }
  


   ngOnInit(): void {
  }



}









  

/*
//תרגיל בית 7
  myForm: FormGroup;
  error = false;
  errorMessage = '';

  constructor(private fb: FormBuilder, private userauthService: AuthService) { }

  ngOnInit(): void {
    
    this.myForm = this.fb.group({
      email: ['', Validators.compose([
          Validators.required,
          this.isEmail
      ])],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.compose([
          Validators.required,
          this.isEqualPassword.bind(this)
      ])],
  });
}

isEmail(control: FormControl): {[s: string]: boolean} {
  if (!control.value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      return {noEmail: true};
  }
}

isEqualPassword(control: FormControl): {[s: string]: boolean} {
  if (!this.myForm) {
      return {passwordsNotMatch: true};

  }
  if (control.value !== this.myForm.controls['password'].value) {
      return {passwordsNotMatch: true};
  }
}

onSignup() {
this.userauthService.signupUser(this.myForm.value); //אחרי לחיצת הכפתור סיינאפ תופעל הפונקציית סיינאפיוזר מהסרביס

  }

*/


