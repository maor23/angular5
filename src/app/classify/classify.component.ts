import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})

export class ClassifyComponent implements OnInit { 

  constructor(private route:ActivatedRoute, private classifyService:ClassifyService, private imageService:ImageService) { } // תרגיל בית 4 - פרמטר חובה בראוט
  
  //נגדיר משתנים 
  //NGMODEL-לבין ה NGFOR-חשוב שהשמות של המשתנים יהיו שונים בין ה 
  networks: string[] = ['bbc', 'cnn', 'nbc']; //ngFor-תרגיל בית 4 - נגדיר מערך לכפתורי רדיו בשביל הלולאה של ה
  selectedNetwork; // ng Model-רגיל בית 4 - נגדיר את המשתנה שנציב ב
  //favoriteNetwork: string; //תרגיל בית 4 - לכפתורי רדיו
  
  text:string; //הרצאה 13
  category:string = 'No Category yet' //הרצאה 13
  categoryImage:string; //הרצאה 13


  //הרצאה 13
  //פונקציה שנרשמת לאובסרבל מהסרביס ומפעילה את הפונקציה קלאסיפיי שכתבנו בסרביס
  classify(){
    this.classifyService.classify(this.text).subscribe(
      res => {
        console.log(res);
        this.category = this.classifyService.categories[res];
        this.categoryImage = this.imageService.images[res];
      }
    )
  }
  
  
  ngOnInit(): void {
    this.selectedNetwork = this.route.snapshot.params.network; //רגיל בית 4 - פרמטר חובה בראוט. חייב לתת את השם "נטוורק" בסוף כי זה השם שנתנו, שהגדרנו את הראוט בקובץ של הראוטים, נתפוס את המשתנה מהיואראל      
  }


}






