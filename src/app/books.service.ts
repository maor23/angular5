import { LayoutModule } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'; //הרצאה 9
import { map } from 'rxjs/operators'; //הרצאה 10 



@Injectable({
  providedIn: 'root'
})

export class BooksService {

  constructor(private db:AngularFirestore) { } //הרצאה 10,9 נגדיר אובייקט שיזזה את הדאטה בייס שלנו

//הרצאה 9
//הרצאה 10
//נגדיר משתנים
  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection(`users`); //הרצאה 11 //פשוט עוד דרך במקום לכתוב במקום את הנתיב המלא כמו במחיקה ובעדכון - הולכים לקולייקשן של יוזרס 
 
 
/*
  //הרצאה 10 - פונקציה ששינינו אותה בהרצאה 12
  //פונקציה שמקבלת יוזר איידי בונה רפרנס ומושכת את הספרים מהדאטה בייס לפי הספרים של כל יוזר
  public getBooks(userId){ 
    this.bookCollection = this.db.collection(`users/${userId}/books`)//משתנה שמנתב את הנתיב של הקולקיישן בוקס לתוך הדאטה בייס
    return this.bookCollection.snapshotChanges().pipe(map(//לפייפ הזה נתייחס רק למקרה של הצלחה (ולא גם למקרה של כישלון כמו שהיינו עושים פעמים קודמות עם פייפ) //מאפ זאת פונקציית המרה, שממירה נתונים מסוג אחד לסוג שני//סנאפשוט זאת פונקצייה שמייצרת לי אובסרבל שמאזין לשינויים בקולקיישן של בוקס, ומעדכנת ישירות לפי כל שינוי שיש ברשימת הספרים בדאטה בייס //נחזיר אובסרבל כי זה מה שמחזירים מדאטה בייס, אובסרבל זה בעצם מזרים מידע, אבל ניתן לשנות את המידע הזה ולשנות את המבנה של המידע בעזרת פונציית המאפ
      collection => collection.map(//הקולייקשן זה מה שמייצר האובסרבול, והמאפ עושנ מניפולציה מסויימת על הדוקומנטס השונים, זה שונה מהמאפ שלמעלה//המאפ זאת פונקצייה ששיכת לספריית קוליקשן שונה מהמאפ שבשורה למעלה
        document => {
          const data = document.payload.doc.data(); //כדי שנוציא דאטה מתוך הדוקומנט ושנדע מה הגוף של כל דוקומנט (במקרה של בוקס זה טייטל וסופר)
          data.id = document.payload.doc.id; //נוסיף בצורה דינאמית בנוסף לטייטל ולסופר, גם אידיי לכל ספר// חשוב שנקבל גם את האיידי של כל ספר ספציפי, כדי שבהמשך נוכל לבצע פעולות קראד על כל ספר בנפרד, האיידי לא נחשב חלק מהדאטה לכן נביא אותו בנפרד
          return data; //הפונקצייה תחזיר בסופו של דבר דאטה,שדאטה זה משתנה בעל 3 שדות: טייטל, סופר, ואיידי
        }
      )
    ))
  }
*/

  //הרצאה 12 - העברנו חלק מהפונקציונליות של הפונקציה מכאן לקומפוננט
  public getBooks(userId,startAfter){                    
    this.bookCollection = this.db.collection(`users/${userId}/books`, //משתנה שמנתב את הנתיב של הקולקיישן בוקס לתוך הדאטה בייס
    ref => ref.orderBy('title', 'asc').limit(4).startAfter(startAfter)); //שהקולייקשן יחזור ממויין בסדר עולה לפי טייטל ובהגבלה של מקסימום 4 ספרים, וציון של האלמנט האחרון שאנחנו מושכים כדי שהוא יביא לנו את האלמנט אחד אחריו
    return this.bookCollection.snapshotChanges() //סנאפשוט זאת פונקצייה שמייצרת לי אובסרבל שמאזין לשינויים בקולקיישן של בוקס בדאטה בייס, ומעדכנת ישירות לפי כל שינוי שיש ברשימת הספרים בדאטה בייס //נרשם לאובסרבול של הסנאפשוט, מהקומפוננט
  }

    //תרגיל בית 12
    public getBooks2(userId,endBefore){                    
      this.bookCollection = this.db.collection(`users/${userId}/books`, //משתנה שמנתב את הנתיב של הקולקיישן בוקס לתוך הדאטה בייס
      ref => ref.orderBy('title', 'asc').limit(4).endBefore(endBefore)); //שהקולייקשן יחזור ממויין בסדר עולה לפי טייטל ובהגבלה של מקסימום 4 ספרים, וציון של האלמנט האחרון שאנחנו מושכים כדי שהוא יביא לנו את האלמנט אחד אחריו
      return this.bookCollection.snapshotChanges() //סנאפשוט זאת פונקצייה שמייצרת לי אובסרבל שמאזין לשינויים בקולקיישן של בוקס בדאטה בייס, ומעדכנת ישירות לפי כל שינוי שיש ברשימת הספרים בדאטה בייס //נרשם לאובסרבול של הסנאפשוט, מהקומפוננט
    }







  //הרצאה 10 נכתוב פונקצייה שמוחקת ספר כדי לעדכן את הדאטה בייס
  deleteBook(Userid:string,id:string){ //הפונקצייה חייבת לקבל גם את האיידי של הספר וגם את האיידי של היוזר - זה בסיס נתונים לא רלציוני לא לשכוח לכן חייב גם את האיידי של יוזר
    this.db.doc(`users/${Userid}/books/${id}`).delete();//נכניס לדוק את הנתיב שיוביל לספר, כך נעדכן את הדאטה בייס
  }



  //הרצאה 11 נכתוב פונקצייה עדכון כדי לעדכן את הדאטה בייס
  updateBook(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update( //נכניס לדוק את הנתיב שיוביל לספר, כך נעדכן את הדאטה בייס
      {
        title:title,
        author:author
      }
    )
  }


  //תרגיל בית 11
  //נוסיף פונקציה של הוספת ספר כדי לעדכן את הדאטה בייס
  //את הפונקציה אפשר לכתוב דומה לפונקציית העדכון (רק בלי האיידי של הספר, כי את האיידי הדאטה בייס נותן לבד לכל ספר), רוני פשוט רצה להראות עוד דרך
  addBook(userId:string,title:string,author:string){ //מה שצריך כדי לעדכן את הדאטה בייס זה מי היוזר שמוסיף ספר, מה הכותרת של הספר ומיהו הסופר 
    const book = {title:title, author:author}; //נגדיר משתנה קבוע של ספר
    this.userCollection.doc(userId).collection('books').add(book); //פשוט עוד דרך במקום לכתוב במקום את הנתיב המלא כמו במחיקה ובעדכון,כמו שבנוי הדאטה בייס: פונים לקולקיישן יוזרס, ואז לדוקומנט יוזר איידי, ואז לקולקיישן בוקס, ומוסיפים לשם את הספר
  }
  







  
  /*
  //הרצאה 9
  getBooks(userId):Observable<any[]>{ //פונקצייה שמושכת את הספרים ליוזר מסוים, הפונקצייה מחזירה אובסרבל
  }
  */
 
  
/* 
  // הרצאה 4
  
  //מערך ג'ייסון של הספרים 
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley"},
  {title:'War and Peace', author:'Leo Tolstoy', summary:"aof type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in"},
  {title:'The Magic Mountain', author:'Thomas Mann', summary:"the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"}]; 

  
  // הפונקציה הראשונה שרוני כתב שמושכת את המידע על הספרים מהסרביס ונקרא לה מהקומפוננט
  public getBooks(){
    return this.books;
  }
  
  
  // רוני רצה ללמד עוד דרך לכתוב את הפונקצייה - באמצעות יצירת אובסראבל על מנת ליצר תהליך אה-סנכרוני
  //ניצור אובסרבל ונרשם אליו מהקומפוננט
  //ניצור פונקציה אה-סינכרונית עם דילאיי שבתוכה ניצור אובסרבאבל - כדי שאחרי שנרשמים לאובסרבאבל הוא מחכה חצי שנייה ואז שולח את רשימת הספרים
  public getBooks(){ 
    const booksObservable = new Observable(obs => { //נשתמש באררו פונקשין  - דרך כתיבה מהירה יותר לכתיבת פונקציה
      setInterval(()=>obs.next(this.books),500) // ה500 זה הדיילאי שנרצה שיהיה במיל שניות, הפונקצייה נקסט היא זו שמרגישה אם נעשה שינוי או הוספה של ספר חדש
  });
  return booksObservable; //נחזיר את המשתנה מסוג אובסרבל הקבוע שיצרנו
}
  
//נוסיף עוד פונקציה של הוספת ספר 
//// סתם רוני רצה להראות לנו איך עושים פוש ואיך זה מוסיף ספר חדש כל 2 שניות לכל מי שנרשם לאובסרבאבל
//ברגע שנוסיף אלמנטים חדשים לבוקס, האוברסראבל ישדר את השינוי לכל מי שנרשם לאותו אובסרסאבל - הספרים יתווספו כ-פוש כל שתי שניות
//לפונקציה אין ריטרן, היא לא מעבירה שום דבר, היא רק מוסיפה ספרים, האובסרבל בזכות הפונקציה נקסט שבפונקציה למעלה, היא מרגישה שיש שינוי באלמנט של הפסרים, והיא תשדר את השינוי לכל מי שרשום לאובסרבל
public addBooks(){
    setInterval(()=>this.books.push({title:'A new one',author:'New Authoe',summary:'Short Summary'}),2000); //אלפיים מיל שניות זה שתי שניות
  } 
  
*/


}
