import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Posts } from '../interfaces/posts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit {

  panelOpenState = false;  //הוספנו בשביל התפריט אקורדיון - העתקתי מהבוקס 
  
  
  constructor(private postService:PostsService) { } //נגדיר משתנה בשם פוסט סרביס מסוג פוסט סרביס

  //נגדיר משתנים
  posts;  
  postsData$:Observable<Posts>; //משתנה שאליו נחבר אובסרבל
  hasError:Boolean = false; // הדיפולטיבי זה שאין שגיאה
  errorMessage:string; 

  
  ngOnInit(): void {
    this.postsData$ = this.postService.searchPostData(); //נגיע לפונקציה סארצ' פוסט דאטה
    this.postsData$.subscribe( // נרשם לאובסרבל - למרות שנרשמנו גם דרך הטיימפלייט - בפתרון של רוני לתרגיל בית הוא וויתר על כל זה כי הוא עשה את הסאסקרייב ישר מהטיימפלייט 
      data => { // כאשר אין שגיאה הפונקצייה סאסקרייב תפעיל את זה ותראה את הנתונים
        this.posts = data;
      },
      error =>{ //כאשר יש שגיאה הפונקציה סאסקרייב תפעיל את זה
        console.log(error.message)//נדפיס את השגיאה לקונסול
        this.hasError = true; //נשנה מהערך הדיפולטיבי פאלס לטרו, משמע יש שגיאה
        this.errorMessage = error.message;
    }
      ) 

  }

}
