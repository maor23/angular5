import { environment } from './../environments/environment.prod'; //הרצאה 7
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'// תרגיל בית 4 //תרגיל בית 7
import { HttpClientModule } from '@angular/common/http'; //תרגיל בית 5

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion'; //הוספנו הרצאה 3
import { MatCardModule } from '@angular/material/card'; // הוספנו תרגיל בית 3
import { MatRadioModule } from '@angular/material/radio'; //תרגיל בית 4
import { MatSelectModule } from '@angular/material/select'; // הרצאה 6
import { MatInputModule} from '@angular/material/input'; // הרצאה 7

import { TemperaturesComponent } from './temperatures/temperatures.component';
import { ClassifyComponent } from './classify/classify.component';
import { CityFormComponent } from './city-form/city-form.component';
import { PostsComponent } from './posts/posts.component';
import { AngularFireModule } from '@angular/fire'; //הרצאה 7
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component'; //הרצאה 7
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { BookFormComponent } from './book-form/book-form.component'; //הרצאה 9


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    TemperaturesComponent,
    ClassifyComponent,
    CityFormComponent,
    PostsComponent,
    LoginComponent,
    RegisterComponent,
    BookFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule, // הוספנו הרצאה 3
    MatCardModule,  // הוספנו תרגיל בית 3
    MatRadioModule, //תרגיל בית 4
    FormsModule, //תרגיל בית 4
    HttpClientModule, //תרגיל בית 5
    MatSelectModule, // הרצאה 6
    AngularFireModule.initializeApp(environment.firebaseConfig), //הרצאה 7
    AngularFireAuthModule, //הרצאה 7
    MatInputModule, //הרצאה 7
    ReactiveFormsModule, //תרגיל בית 7
    AngularFirestoreModule, //הרצאה 9
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }