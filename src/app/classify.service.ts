import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

constructor(private http:HttpClient) { }


//נוסיף משתנים
private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta"; //האיפייאיי של רוני
categories:object = {0:'Business', 1:'Entertainment', 2:'Politics', 3:'Sport', 4:'Tech'}; //משתנה מערך ג'ייסון של הקטגוריות השונות שלנו - נקבל מספרים ונצטרך להמיר אותם לקטגוריות - המבנה לקוח מהקוד של הפייתון באמאזון 


//פונקציה שמקבלת טקסט, ובונה את הג'ייסון של הבקשת אייפיאיי
classify(text:string){
  let json = {'articles':[ //חייב לוודא שזה המבנה שהג'ייסון שבאיייפיאיי רוצה לקבל
    {'text':text}
  ]} 
  let body = JSON.stringify(json);  //נעביר לסטרינג כדי שיוכל לעבור לאמאזון דרך צינורות האינרטנט
  return this.http.post<any>(this.url, body).pipe( //נחזיר אובסרבל מסוג אהני כי התעצלנו לבנות אינטרפייס
    map(res => {
      console.log(res);
      let final:string = res.body; //פיינל זה המשתנה שאנחנו צריכים להעביר לקומפוננט
      console.log(final);
      final = final.replace('[', ''); //נעשה שינוי על הסטרינג כדי שיחזור בלי הסוגריים
      final = final.replace(']', '');
      return final;
    })
  ) 
}



}
