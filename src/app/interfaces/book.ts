export interface Book { //אינטרפייס של ספרים
    id: string,
    title: string,
    author: string,
    summary?: string, //אופציונלי
}
