export interface User { //ניצור אינטרפייס כדי להגדיר את מבנה הנתונים של כל יוזר
    uid: string,
    email?: string | null,
    photoUrl?: string,
    displayName?: string
    //email:string; //תרגיל בית 7
    //password:string; //תרגיל בית 7
    //confirmpassword:string; //תרגיל בית 7
}
