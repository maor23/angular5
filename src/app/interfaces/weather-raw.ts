export interface WeatherRaw { //סכמת נתונים בשפת ג'ייסון  - לא צריך פסיק בסוף ולא מרכאות, שמות המשתנים הם כמו שבאתר מזג האוויר - אינטרפייס זמני
        coord:{
           lon:number
           lat:number
        },
        weather:[ //מערך
           {
              description:string
              icon:string
           }
        ],
        main:{
           temp:number
        },
        sys:{
           country:string
        },
        name:string


}
