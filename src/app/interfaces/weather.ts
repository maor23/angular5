export interface Weather { //אינטרפייס סופי של המבנה נתונים שנרצה - כאן גם ניתן להביא איזה שמות של משתנים שנרצה
    name:string,
    country:string,
    image:string,
    description:string,
    temperature:number,
    lat?:number, //משתנה אופציונלי
    lon?:number //משתנה אופציונלי
}
