import { Observable } from 'rxjs';
import { WeatherService } from './../weather.service'; // התווסף אוטומטית
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})

export class TemperaturesComponent implements OnInit { 
  
  constructor(private route:ActivatedRoute, private weatherService:WeatherService) { } // הרצאה 4 והרצאה 5
  
  //נגדיר את המשתנים ואת הסוג שלהם
  city:string; 
  temperature:number; 
  image:string;
  country:string;  
  lat:number; 
  lon:number;  
  weatherData$:Observable<Weather>; //מוסכמה של אובסרבל שאם מוסיפים דולר למשתנה - זה אומר שהמשתנה מסוג אובסרבל. המידע שהאובסרבל יפלוט יהיה מסוג ווטר 
  hasError:Boolean = false; // הדיפולטיבי זה שאין שגיאה
  errorMessage:string; 

  


  ngOnInit(): void {    
    //משיכת הפרמטר חובה מה-יו אר אל
    this.city = this.route.snapshot.params.city; // נתנו את השם סיטי בסוף כי זה השם שהגדרנו בקובץ של אפפ-ראוטינג,נתפוס את המשתנה מהיואראל
    
    this.weatherData$ = this.weatherService.searchWeatherData(this.city);  //הפונקציה סראצ'ווטרדאטה שנמצאת בסרביס, מחזירה אובסרבל  
    this.weatherData$.subscribe( // נרשם לאובסרבל, בשביל שכשיגיעו נתונים נוכל לתפוס אותם
      data => { // כאשר אין שגיאה הפונקצייה סאסקרייב תפעיל את זה ותראה את הנתונים
        this.temperature = data.temperature; //בגלל השם שנתנו לפונקציה בשורה למעלה בשם "דאטה", גם כאן צריך לכתוב דאטה
        this.image = data.image;
        this.country = data.country;
        this.lat = data.lat;
        this.lon = data.lon;
      },
      error =>{ //כאשר יש שגיאה הפונקציה סאסקרייב תפעיל את זה
        console.log(error.message)//בגלל השם שנתנו לפונקציה בשורה למעלה בשם "ארור", גם כאן צריך לכתוב ארור, נדפיס את השגיאה לקונסול
        this.hasError = true; //נשנה מהערך הדיפולטיבי פאלס, לטרו, משמע יש שגיאה
        this.errorMessage = error.message;
      }
    ) 
  }




}
